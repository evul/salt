### AWS master IP:
172.31.24.60

### Install AWS master:
sudo su

git clone https://gitlab.com/evul/salt.git && cd salt && bash setup_salt_master.sh

### Install AWS minion:
sudo su

git clone https://gitlab.com/evul/salt.git && cd salt && bash setup_salt_minion.sh