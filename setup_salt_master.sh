#!/usr/bin/env bash

# Root permissions are required to run this script
if [ `whoami` != "root" ]; then
    echo "Salt requires root privileges to install. Please re-run this script as root."
    exit 1
fi

while ! [ ! -z "$name" ]
do
  echo "Enter minion name:"
  read -r name
done

sudo sh ./install_salt.sh -P -M

IP=`hostname -I`

sudo echo "interface: ${IP}" >> /etc/salt/master
sudo echo "auto_accept: True" >> /etc/salt/master
sudo systemctl restart salt-master

sudo echo "${name}" > /etc/salt/minion_id
sudo hostname -b "${name}"
sudo echo "master: ${IP}" >> /etc/salt/minion
sudo systemctl restart salt-minion